<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    {{-- <x-jet-welcome /> --}}
        @if (Auth::user()->roleuser->id == 2)
            <div class="row justify-content-center my-5">
                <div class="col-md-12">
                    <div class="card shadow bg-light">
                        <div class="card-body bg-white px-5 py-3 border-bottom rounded-top">
                            <h3 class="h3 my-4">
                                Bienvenido al Panel Administrador!
                            </h3>
                            <img src="{{asset('images/club.png')}}" height="200" width="500">
                        </div>
                    </div>
                </div>
            </div>
        @endif

</x-app-layout>
