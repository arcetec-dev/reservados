<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Ingreso de Datos') }}
        </h2>
    </x-slot>
        @livewire('inputdata')
</x-app-layout>
