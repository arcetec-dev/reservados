<button {{ $attributes->merge(['type' => 'submit', 'class' => 'btn btn-reservados text-uppercase']) }}>
    {{ $slot }}
</button>
