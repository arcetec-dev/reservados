<div>
    {{-- The best athlete wants his opponent at his best. --}}
    Desde livewire!
    <x-jet-button class="mb-4" wire:click="toggleUserModal(1)"> Agregar Nuevo Usuario </x-jet-button>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->lastname}}</td>
                    <td>
                        <x-jet-button class="btn btn-info" wire:click="toggleUserModal(2, {{$user->id}})"> <i class="fa fa-eye"></i> </x-jet-button>
                        <x-jet-button class="btn btn-info" wire:click="toggleUserModal(3, {{$user->id}})"> <i class="fa fa-pen"></i> </x-jet-button>
                        <x-jet-button class="btn btn-info" wire:click="toggleUserModal(4, {{$user->id}})"> <i class="fa fa-trash"></i> </x-jet-button>
                    </td>
                </tr>
        @endforeach
        </tbody>
    </table>
    {{ $users->links() }}


    <x-jet-dialog-modal wire:model="userModal" maxWidth="lg"> <x-slot name="title"> Add New User = {{$user_id}} </x-slot>
        <x-slot name="content">
            {{--Agregar Usuario--}}
            @if($agregando || $editando)
            <div class="mt-4"> 
                <x-jet-label for="name" >* Nombre completo</x-jet-label>
                <x-jet-input type="text" class="mt-1 block w-full" wire:model="name" id="name"/>
                @error('name')
                Requerido
                @enderror


            </div>
            <div class="mt-4">
                <x-jet-label for="email" >* Email Address</x-jet-label>
                <x-jet-input type="text" class="mt-1 block w-full" wire:model="email" />
                <x-jet-input-error for="email" class="mt-2" />
                @error('email')
                Requerido
                @enderror

            </div>
            <div class="mt-4">
                <x-jet-label for="" >* Password</x-jet-label>
                <x-jet-input type="password" class="mt-1 block w-full" wire:model="password"/>
                <x-jet-input-error for="text" class="mt-2" />
                @error('password')
                Requerido
                @enderror

            </div>
            @endif
            @if($viendo || $eliminando)
                <div class="mt-4">
                    <x-jet-label class="mt-1 block w-full">Nombre Completo: {{$name}}</x-jet-label>
                </div>
                <div class="mt-4">
                    <x-jet-label class="mt-1 block w-full">Email          : {{$email}}</x-jet-label>
                </div>
            @endif
        </x-slot>
        <x-slot name="footer">
            @if($agregando || $editando)
                <x-jet-button class="ml-2" wire:click="addUser" wire:loading.attr="disabled">
                    {{ __('Guardar') }}
                </x-jet-button>
            @endif
            @if($eliminando)
                <x-jet-label>Seguro que desea Eliminar</x-jet-label>
                <x-jet-button class="ml-2" wire:click="delete" wire:loading.attr="disabled">
                    {{ __('Eliminar') }}
                </x-jet-button>
            @endif
            <x-jet-secondary-button wire:click="$toggle('userModal')" wire:loading.attr="disabled">
                    {{ __('Cancelar') }}
            </x-jet-secondary-button>
        </x-slot>
    </x-jet-dialog-modal>
</div>
