<div>
    <div class="col-12 row">
        <div class="col-8">
            <div class="d-flex align-items-center text-white-50 rounded box-shadow">
                <img src="{{ asset('images/club.png') }}" class="mx-4" height="100" width="350">
            </div>
            <h1 class="mx-4">REGISTRO DE INMUEBLE Y <span class="text-warning">NÚCLEO FAMILIAR</span></h1>
            <h4>Informacion del propietario:</h4>
            <div class="input-group mb-2">
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Conjunto</label>
                </div>
                <select class="input-group form-select" wire:model="srid">
                    <option value="">Seleccione Conjunto</option>
                    @foreach ($subresidentials as $subresidential)
                        <option value="{{ $subresidential->id }}">{{ $subresidential->name }}</option>
                    @endforeach
                </select>
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Torre</label>
                </div>
                <select class="input-group form-select" wire:model="b_id">
                    <option>Seleccione Torre</option>
                    @if ($buildings)
                        @foreach ($buildings as $building)
                            <option value="{{ $building->id }}">{{ $building->title }}</option>
                        @endforeach
                    @endif
                </select>
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Apartamento</label>
                </div>
                <select class="input-group form-select" wire:model="apartmentid">
                    <option>Seleccione Apartamento</option>
                    @if ($apartments)
                        @foreach ($apartments as $apartment)
                            <option value="{{ $apartment->id }}">{{ $apartment->number }}</option>
                        @endforeach
                    @endif
                </select>

            </div>
            <div class="input-group m-2">
                {{-- Si ha elegido un apartamento valido --}}
                @if ($aptovalido)
                    @if (!$asignado)
                        <button class="btn-success p-2" wire:click="newedit()"><i class="fa fa-user-plus"></i> Habilitar
                            Propietario</button>
                    @endif
                    {{-- Si NO pertenece a reservadosS --}}
                    @if ($asignado)

                        <button class="form-control btn-reservados m-2" wire:click="edit()"><i
                                class="fa fa-pencil-alt"></i>
                            Editar Propietario</button>
                        <button class="form-control btn-reservados m-2" wire:click="add_r()"><i
                                class="fa fa-user-plus"></i>
                            Agregar Habitante</button>

                    @endif
                @endif
            </div>
            @if (!$editando)
                <div class="input-group m-2">
                    {{-- Si esta Consultando --}}

                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Estado</label>
                    </div>
                    <select class="input-group form-select" wire:model="statusid" disabled="true">
                        <option>Estado del Inmueble</option>
                        @foreach ($statuses as $status)
                            <option value="{{ $status->id }}">{{ $status->title }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Nombres</label>
                    </div>
                    <input type="text" class="form-control" placeholder="* Nombre" wire:model="name" disabled="true">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Apellidos</label>
                    </div>
                    <input type="text" class=" form-control" placeholder="* Apellido" wire:model="lastname"
                        disabled="true">
                </div>
                <div class="input-group m-2 w-full">

                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Nacimiento</label>
                    </div>
                    <input type="date" class="input-group-text" wire:model="userbirth" disabled="true">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Género</label>
                    </div>
                    <select class="input-group form-select" wire:model="genderid" disabled="true">
                        <option>Genero</option>
                        @foreach ($genders as $gender)
                            <option value="{{ $gender->id }}">{{ $gender->title }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Tipo Documento</label>
                    </div>
                    <select class="input-group form-select" wire:model="doctypeid" disabled="true">
                        <option>Tipo Documento</option>
                        @foreach ($docs as $doc)
                            <option value="{{ $doc->id }}">{{ $doc->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group m-2">

                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text"># Documento</label>
                    </div>
                    <input type="text" class="input-group-text form-control" placeholder="* Numero de documento"
                        wire:model="userdocument" disabled="true">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text"># Móvil</label>
                    </div>
                    <input type="text" class="input-group-text  form-control" placeholder="* Celular" wire:model="phone"
                        disabled="true">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">eMail</label>
                    </div>
                    <input type="email" class="input-group-text  form-control" placeholder="* Correo Electronico"
                        wire:model="email" disabled="true">
                </div>
                <div class="input-group m-2">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Usuario</label>
                    </div>
                    <input type="text" class=" form-control" placeholder="* Usuario" wire:model="username"
                        disabled="true">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Motriz reducida</label>
                    </div>
                    <select class="input-group form-select" wire:model="userdisabilityid" disabled="true">
                        @foreach ($disabilities as $disability)
                            <option value="{{ $disability->id }}">{{ $disability->title }}</option>
                        @endforeach
                    </select>
                </div>
            @else {{-- Si le dio click a Editar --}}
                <div class="input-group m-2">
                    {{-- Si esta Consultando --}}

                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Estado</label>
                    </div>
                    <select class="input-group form-select" wire:model="newstatusid">
                        <option>Estado del Inmueble</option>
                        @foreach ($statuses as $status)
                            <option value="{{ $status->id }}">{{ $status->title }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Nombres</label>
                    </div>
                    <input type="text" class="form-control" placeholder="* Nombre" wire:model="newname">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Apellidos</label>
                    </div>
                    <input type="text" class=" form-control" placeholder="* Apellido" wire:model="newlastname">
                </div>
                <div class="input-group m-2 w-full">

                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Nacimiento</label>
                    </div>
                    <input type="date" class="input-group-text" wire:model="newuserbirth">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Género</label>
                    </div>
                    <select class="input-group form-select" wire:model="newgenderid">
                        <option>Genero</option>
                        @foreach ($genders as $gender)
                            <option value="{{ $gender->id }}">{{ $gender->title }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Tipo Documento</label>
                    </div>
                    <select class="input-group form-select" wire:model="newdoctypeid">
                        <option>Tipo Documento</option>
                        @foreach ($docs as $doc)
                            <option value="{{ $doc->id }}">{{ $doc->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group m-2">

                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text"># Documento</label>
                    </div>
                    <input type="text" class="input-group-text form-control" placeholder="* Numero de documento"
                        wire:model="newuserdocument">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text"># Móvil</label>
                    </div>
                    <input type="text" class="input-group-text  form-control" placeholder="* Celular"
                        wire:model="newphone">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">eMail</label>
                    </div>
                    <input type="email" class="input-group-text  form-control" placeholder="* Correo Electronico"
                        wire:model="newemail">
                </div>
                <div class="input-group m-2">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Usuario</label>
                    </div>
                    <input type="text" class=" form-control" placeholder="* Usuario" wire:model="newusername"
                        disabled="true">
                    <div class="input-group-prepend mx-1">
                        <label class="input-group-text">Motriz reducida</label>
                    </div>
                    <select class="input-group form-select" wire:model="newuserdisabilityid">
                        @foreach ($disabilities as $disability)
                            <option value="{{ $disability->id }}">{{ $disability->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group m-2">
                    @if ($editnew)
                        <button type="submit" class="form-control btn-reservados m-2" wire:click="savenew()"><i
                                class="fa fa-save"></i> Asignar</button>
                    @else
                        <button type="submit" class="form-control btn-reservados m-2" wire:click="save()"><i
                                class="fa fa-save"></i> Guardar</button>
                    @endif
                    <button class="form-control btn-reservados m-2" wire:click="cancel()"><i
                            class="fa fa-times"></i>
                        Cancelar</button>
                </div>
            @endif
        </div>
        <div class="col-4 my-5 bg-reservados">
            <label class="text-white">Registre la información del miembro del núcleo familiar que reside en el
                apartamento
                registrado. Favor tener en cuenta que sobre este registro se va a realizar la reservacion del club
            </label>

            <h4 class="text-white p-1">RESIDENTES DEL APARTAMENTO:</h4>
            <label class="text-white">* Habitantes del inmueble (Copropietario o Arrendatario)</label>
            <div class="input-group mb-2">
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Tipo Documento</label>
                </div>
                <select class="input-group form-select" wire:model="relativedoctype">
                    <option value="">Seleccione...</option>
                    @foreach ($docs as $doc)
                        <option value="{{ $doc->id }}">{{ $doc->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group mb-2">
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Género</label>
                </div>
                <select class="input-group form-select" wire:model="relativegenderid">
                    <option>Genero</option>
                    @foreach ($genders as $gender)
                        <option value="{{ $gender->id }}">{{ $gender->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group mb-2">
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Nombre</label>
                </div>
                <input type="text" class="form-control" placeholder="Nombre" wire:model="relativename">
                @error('relativename') <span class="text-white"> Requerido</span> @enderror
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Apellido</label>
                </div>
                <input type="text" class="form-control" placeholder="Apellido" wire:model="relativelastname">
                @error('relativelastname') <span class="text-white"> Requerido</span> @enderror
            </div>
            <div class="input-group mb-2">
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Telefono</label>
                </div>
                <input type="text" class="form-control" placeholder="Telefono" wire:model="relativephone">
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">eMail</label>
                </div>
                <input type="text" class="form-control" placeholder="Correo Electronico" wire:model="relativeemail">
            </div>
            <div class="input-group mb-2">
                <div class="input-group-prepend mx-1">
                    <label class="input-group-text">Fecha Nacimiento</label>
                </div>
                <input type="date" class="form-control" wire:model="relativebirth">
                @error('relativebirth') <span class="text-white"> Requerido</span> @enderror
            </div>
            @if ($adding_r)
                <div class="input-group mb-2">
                    <button class="form-control btn-reservados m-2" wire:click="save_r()"><i
                            class="fa fa-save"></i>
                        Guardar</button>
                    <button class="form-control btn-reservados m-2" wire:click="cancel_r()"><i
                            class="fa fa-times"></i> Cancelar</button>
                </div>
            @endif
        </div>
        <div class="container col-md-12 ">
            <div class="row col-8 col-offset-1">
                <h3 class="text-warning">Residentes del Apartamento</h3>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellido</th>
                            <th scope="col">F.Nacimiento</th>
                            <th scope="col">Genero</th>
                            <th scope="col">Discapacidad</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($relatives as $relative)
                            <tr>
                                <td>{{ $relative->name }}</td>
                                <td>{{ $relative->lastname }}</td>
                                <td>{{ $relative->birth }}</td>
                                <td>{{ $relative->gender->title }}</td>
                                <td>{{ $relative->disability->title }}</td>
                                <td>
                                    <div class="form-inline">
                                        <button title="Ver datos" class="m-1"
                                            wire:click="view_r({{ $relative->id }})"><i
                                                class="fa fa-eye text-info"></i></button>
                                        <button title="Editar" class="m-1"
                                            wire:click="edit_r({{ $relative->id }})"><i
                                                class="fa fa-edit text-success"></i></button>
                                        <button title="Eliminar" class="m-1"
                                            onclick="return confirm('Desea Eliminar Registro?');"
                                            wire:click="delete_r({{ $relative->id }})">
                                            <i class="fa fa-trash text-danger"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
