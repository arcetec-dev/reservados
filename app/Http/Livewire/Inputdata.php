<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Gender;
use App\Models\User;
use App\Models\UserRelative;
use App\Models\SubResidential;
use App\Models\Building;
use App\Models\Apartment;
use App\Models\PropertyStatus;
use App\Models\DocumentType;
use App\Models\Disability;

class Inputdata extends Component
{
    public $genders,
    $subresidentials,
    $buildings,
    $apartments,
    $statuses,
    $relatives,
    $disabilities,
    $srid,
    $b_id,
    $apartmentid,
    $texto,
    $editando,
    $asignado,
    $aptovalido,
    $adding_r,
    $idr,
    $habilitado,
    $newuser,
    $editnew,
    $apartmentnumber,
    $name, $lastname, $phone, $email, $username, $userdocument, $userbirth,
    $genderid, $doctypeid, $propertystateid, $userdisabilityid, $statusid, $userselected_id,

    $newgenderid, $newdoctypeid, $newpropertystateid, $newname, $newlastname, $newphone, $newemail, $newuserbirth,
    $newusername, $newuserdocument, $newuserdisabilityid, $newstatusid,
    $relativename, $relativelastname, $relativegenderid, $relativedoctype, $relativephone,
    $relativeemail, $relativedocument, $relativedisabilityid, $relativebirth;

    protected $rules = [
        'newname' => 'required',
        'newlastname' => 'required',
        'newuserbirth' => 'required',
        'newemail' => 'required|email',
        'newuserdocument' => 'required',
        'newdoctypeid' => 'required',
    ];


    public function mount()
    {
        $this->subresidentials = SubResidential::all();
        $this->genders = Gender::all();
        $this->statuses = PropertyStatus::all();
        $this->docs = DocumentType::all();
        $this->disabilities = Disability::all();
        $this->buildings = [];
        $this->apartments = [];
        $this->relatives = [];
        $this->editando=false;
        $this->asignado = false;
        $this->aptovalido = false;
        $this->adding_r = false;
        $this->habilitado = false;
        $this->idr = null;
        $this->userbirth = today();
        $this->newuserbirth = today();
        $this->relativebirth = today();
    }
    public function render()
    {
        if($this->srid)
        {
            $this->buildings = Building::where('subresidential_id', $this->srid)->get();
            if($this->buildings)
            {
                $this->apartments = Apartment::where('building_id', $this->b_id)->get();
            }
        }
        else
        {
            $this->buildings = null;
            $this->apartments = null;
            $this->apartmentid = 0;
        }
        if($this->apartmentid)
        {
            $this->aptovalido = true;
            $apartamento = Apartment::where('id', $this->apartmentid)->first();
            $this->statusid = $apartamento->ps_id;
            $this->userselected_id = $apartamento->user_id;
            $this->apartmentnumber = $apartamento->number;
            if($apartamento->user_id==2)
            {
                $this->asignado = false;
                $this->clear();
            }
            else
            {
                $owner = User::where('id', $apartamento->user_id)->first();
                $this->asignado=true;
                $this->name = $owner->name;
                $this->lastname = $owner->lastname;
                $this->phone = $owner->phone;
                $this->email = $owner->email;
                $this->username = $owner->username;
                $this->userbirth = $owner->birth;
                $this->userdisabilityid = $owner->disability_id;
                $this->genderid = $owner->gender_id;
                $this->doctypeid = $owner->documenttype_id;
                $this->userdocument = $owner->document_number;
                $this->relatives = UserRelative::where('user_id', $apartamento->user_id)
                ->with('gender')
                ->with('disability')
                ->with('documenttype')
                ->get();
            }
        }
        else
        {
            $this->aptovalido = false;
        }

        return view('livewire.inputdata');
    }

    public function clear()
    {
        $this->genderid = null;
        $this->doctypeid = null;
        $this->propertystateid=null;
        $this->userdisabilityid=null;
        $this->name=null;
        $this->lastname=null;
        $this->phone=null;
        $this->email=null;
        $this->username=null;
        $this->userbirth=null;
        $this->relatives=[];
    }

    public function cancel()
    {
        $this->editando = false;
        $this->habilitado = true;
        $this->clear();
    }

    public function edit()
    {
        $this->newname = $this->name;
        $this->newgenderid = $this->genderid;
        $this->newstatusid = $this->statusid;
        $this->newlastname = $this->lastname;
        $this->newuserdocument = $this->userdocument;
        $this->newphone = $this->phone;
        $this->newemail = $this->email;
        $this->newuserbirth = $this->userbirth;
        $this->newuserdisabilityid = $this->userdisabilityid;
        $this->newdoctypeid = $this->doctypeid;
        $this->habilitado = false;
        $this->editando = true;
        $this->editnew = false;
        $this->newusername = "R".$this->srid."T".$this->b_id.$this->apartmentnumber;

    }

    public function newedit()
    {
        $this->newname = $this->name;
        $this->newgenderid = $this->genderid;
        $this->newstatusid = $this->statusid;
        $this->newlastname = $this->lastname;
        $this->newuserdocument = $this->userdocument;
        $this->newphone = $this->phone;
        $this->newemail = $this->email;
        $this->newuserbirth = $this->userbirth;
        //$this->newusername = $this->username;
        $this->newuserdisabilityid = $this->userdisabilityid;
        $this->newdoctypeid = $this->doctypeid;
        $this->habilitado = false;
        $this->editando = true;
        $this->editnew = true;
        $this->newusername = "R".$this->srid."T".$this->b_id.$this->apartmentnumber;
    }





    public function save()
    {
        if(!$this->newgenderid){$this->newgenderid=1;}
        if(!$this->newuserdisabilityid){$this->newuserdisabilityid=1;}
        if(!$this->newdoctypeid){$this->newdoctypeid=1;}
        //if(!$this->newuserbirth){$this->newuserbirth=today();}
        if(!$this->newusername){$this->newusername="SinUsuario";}

        $this->validate();

        $apartment = Apartment::find($this->apartmentid);
        $apartment->update([
          'ps_id' => $this->statusid,
        ]);

        $user = User::find($apartment->user_id);
        $user->update([
            'name' => $this->newname,
            'lastname' => $this->newlastname,
            'username' => $this->newusername,
            'email' => $this->newemail,
            'phone' => $this->newphone,
            'birth' => $this->newuserbirth,
            'gender_id' => $this->newgenderid,
            'documenttype_id' => $this->newdoctypeid,
            'document_number' => $this->newuserdocument,
            'disability_id' => $this->newuserdisabilityid,
        ]);
        $this->habilitado = true;
        $this->editando = false;
        session()->flash('status', 'Guardado Correctamente.');
    }

    public function savenew()
    {
       // $this->validate();
       if(!$this->newgenderid){$this->newgenderid=1;}
       if(!$this->newusername){$this->newusername="SinUsuario";}
       if(!$this->newuserdisabilityid){$this->newuserdisabilityid=1;}
       if(!$this->newdoctypeid){$this->newdoctypeid=1;}
       if(!$this->newuserbirth){$this->newuserbirth=today();}
       $password = bcrypt('Reservados');

       $this->validate();

        $user = User::create(
        [
            'name' => $this->newname,
            'lastname' => $this->newlastname,
            'username' => $this->newusername,
            'email' => $this->newemail,
            'phone' => $this->newphone,
            'password' => $password,
            'birth' => $this->newuserbirth,
            'gender_id' => $this->newgenderid,
            'documenttype_id' => $this->newdoctypeid,
            'document_number' => $this->newuserdocument,
            'disability_id' => $this->newuserdisabilityid,
        ]);
        $user->save();
        $iduser = $user->id;

        $apartment = Apartment::find($this->apartmentid);
        $apartment->update([
            'user_id' => $iduser,
            'ps_id' => $this->statusid,
        ]);

        $this->habilitado = true;
        $this->editando = false;
        session()->flash('status', 'Guardado Correctamente.');
    }

    public function clear_r()
    {
        $this->relativedoctypeid = null;
        $this->relativedocument=null;
        $this->relativegenderid = null;
        $this->relativename=null;
        $this->relativelastname=null;
        $this->relativephone=null;
        $this->relativeemail=null;
        $this->relativebirth=null;
        $this->relativedisabilityid=null;
    }

    public function edit_r($id)
    {
        $this->idr = $id;
        $r = UserRelative::find($id);
        $this->relativename = $r->name;
        $this->relativelastname = $r->lastname;
        $this->relativephone = $r->phone;
        $this->relativedocument = $r->document_number;
        $this->relativebirth = $r->birth;
        $this->relativeemail = $r->email;
        $this->relativedisabilityid = $r->disability_id;
        $this->relativedoctype = $r->documenttype_id;
        $this->relativegenderid =$r->gender_id;
        $this->habilitado = true;
        $this->adding_r = true;
    }

    public function add_r()
    {
        $this->clear_r();
        $this->adding_r = true;
    }

    public function view_r($id)
    {
        $r = UserRelative::find($id);
        $this->relativename = $r->name;
        $this->relativelastname = $r->lastname;
        $this->relativephone = $r->phone;
        $this->relativedocument = $r->document_number;
        $this->relativebirth = $r->birth;
        $this->relativeemail = $r->email;
        $this->relativedisabilityid = $r->disability_id;
    }

    public function cancel_r()
    {
        $this->clear_r();
        $this->adding_r = false;
    }

    public function delete_r($id)
    {
        $this->clear_r();
        $this->adding_r = false;
        $this->idr = null;
        $r = UserRelative::find($id);
        $r->delete();
        session()->flash('status', 'Eliminado Correctamente.');
    }



    public function save_r()
    {
        if(!$this->relativegenderid){$this->relativegenderid=1;}
        if(!$this->relativedisabilityid){$this->relativedisabilityid=1;}
        if(!$this->relativedoctype){$this->relativedoctype=1;}
        if(!$this->relativebirth){$this->relativebirth=today();}
        $validatedData = $this->validate([
            'relativename' => 'required',
            'relativelastname' => 'required',
            'relativebirth' => 'required',
        ]);

        if($validatedData)
        {
            if($this->idr)
            {
                $relative = UserRelative::find($this->idr);
                $relative->update([
                    'name' => $this->relativename,
                    'lastname' => $this->relativelastname,
                    'birth' => $this->relativebirth,
                    'email' => $this->relativeemail,
                    'phone' => $this->relativephone,
                    'gender_id' => $this->relativegenderid,
                    'documenttype_id' => $this->relativedoctype,
                    'document_number' => $this->relativedocument,
                    'disability_id' => $this->relativedisabilityid,
                ]);
                $relative->save();
                $this->cancel_r();
            }
            else
            {
                //dd($this->relativename);
                //$relative = new UserRelative;
                //$owner = User::where('id', $apartamento->user_id)->first();
                $relative = UserRelative::create([
                    'user_id' => $this->userselected_id,
                    'name' => $this->relativename,
                    'lastname' => $this->relativelastname,
                    'birth' => $this->relativebirth,
                    'gender_id' => $this->relativegenderid,
                    'documenttype_id' => $this->relativedoctype,
                    'document_number' => $this->relativedocument,
                    'email' => $this->relativeemail,
                    'phone' => $this->relativephone,
                    'disability_id' => $this->relativedisabilityid,
                ]);
                $relative->save();
                $this->cancel_r();
            }

        }
    }
}
