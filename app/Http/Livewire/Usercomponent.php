<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;

class Usercomponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    //public $users;
    public $name, $email, $password, $user_id;
    public $userModal = false;
    public $agregando = false;
    public $viendo = false;
    public $editando = false;
    public $eliminando = false;


    protected $rules=[
        'name'=>'required',
        'email'=>'required|email',
        'password'=>'required',
    ];

    public function render()
    {
        //$this->users = User::paginate(10)->get();
        return view('livewire.usercomponent', [
            'users' => User::paginate(10),
        ]);
    }

    public function toggleUserModal($accion, $id = null) {
        // 1 agregando, 2 viendo, 3 editando, 4 eliminando
        $this->userModal = true;
        $this->user_id = $id;
        if($accion==1)
        {
            $this->name = null;
            $this->email = null;
            $this->agregando = true;
            $this->viendo = false;
            $this->editando = false;
            $this->eliminando = false;
        }
        if($accion==2)
        {
            if($id)
            {
                $user = User::find($id);
                $this->name = $user->name;
                $this->email = $user->email;
                $this->viendo = true;
                $this->agregando = false;
                $this->editando = false;
                $this->eliminando = false;
            }
        }
        if($accion==3)
        {
            if($id)
            {
                $user = User::find($id);
                $this->name = $user->name;
                $this->email = $user->email;
                $this->viendo = false;
                $this->agregando = false;
                $this->editando = true;
                $this->eliminando = false;
            }
        }
        if($accion==4)
        {
            if($id)
            {
                $user = User::find($id);
                $this->name = $user->name;
                $this->email = $user->email;
                $this->viendo = false;
                $this->agregando = false;
                $this->editando = false;
                $this->eliminando = true;
            }
        }
    }

    public function view($id)
    {
        $user = User::find($id);
        $this->name = $user->name;
        $this->email = $user->email;
    }

    public function addUser()
    {
      $this->validate();

      if($this->user_id)
      {
        $user = User::find($this->user_id);
        $user->update(
            [
                'name'=> $this->name,
                'email' => $this->email,
            ]);
      }
      else
      {
        User::create([
        'name' => $this->name,
        'email' => $this->email,
        'password' => Hash::make($this->password),
          ]);
      }
      
      $this->clear();
    }

    public function delete()
    {
        if($this->user_id)
        {
            if(Auth::user()->id <> $this->user_id)
            {
                $user = User::find($this->user_id);
                $user->delete();    
            }
            
        }
        $this->clear();
    }

    public function clear()
    {
        $this->userModal = false;
        $this->name=null;
        $this->email=null;
        $this->password=null;
        $this->agregando = false;
        $this->viendo = false;
        $this->editando = false;
        $this->eliminando = false;
    }
}
