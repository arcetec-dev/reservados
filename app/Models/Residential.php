<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Residential extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'logo',
        'picture',
        'address',
        'location',
        'contact_name',
        'contact_email',
        'phone',
        'whatsapp',
        'facebook',
        'twitter',
        'instagram',
        'country_id',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
