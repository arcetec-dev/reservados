<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubResidential extends Model
{
    use HasFactory;

    protected $fillable = [
        'residential_is',
        'name',
        'house',
        'building',
        'mix',
        'picture',
    ];

    public function countries()
    {
        return $this->hasOne(Country::class);
    }
}
