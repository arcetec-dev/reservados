<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRelative extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'lastname',
        'birth',
        'picture',
        'email',
        'phone',
        'user_id',
        'disability_id',
        'gender_id',
        'documenttype_id',
        'document_number',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function disability()
    {
        return $this->belongsTo(Disability::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function documenttype()
    {
        return $this->belongsTo(DocumentType::class);
    }
}
