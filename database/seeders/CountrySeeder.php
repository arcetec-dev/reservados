<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'             => 1,
                'name'           => 'Colombia',
                'code'          => 52,
            ],
            [
                'id'             => 2,
                'name'           => 'El Salvador',
                'code'             => 503,
            ],
        ];

        Country::insert($records);
    }
}
