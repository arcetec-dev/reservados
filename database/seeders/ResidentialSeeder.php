<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Residential;

class ResidentialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'             => 1,
                'country_id'     => 1,
                'name'           => 'Club House Huertas Reservado',
            ],

        ];

        Residential::insert($records);
    }
}
