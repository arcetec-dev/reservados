<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Gender;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'             => 1,
                'title'           => 'Masculino',
            ],
            [
                'id'             => 2,
                'title'           => 'Femenino',
            ],
        ];

        Gender::insert($records);
    }
}
