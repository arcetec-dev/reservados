<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DocumentType;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'             => 1,
                'title'           => 'Cedula',
            ],
            [
                'id'             => 2,
                'title'           => 'Pasaporte',
            ],
        ];

        DocumentType::insert($records);
    }
}
