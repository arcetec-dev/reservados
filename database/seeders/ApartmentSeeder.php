<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Apartment;

class ApartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 1,
                'ps_id'          => 1,
                'number'         => '506',
            ],
            //Segunda Torre
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 2,
                'ps_id'          => 1,
                'number'         => '506',
            ],
            //Tercera Torre
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 3,
                'ps_id'          => 1,
                'number'         => '506',
            ],
            //Cuarta Torre
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 4,
                'ps_id'          => 1,
                'number'         => '506',
            ],
            //Quinta Torre
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 5,
                'ps_id'          => 1,
                'number'         => '506',
            ],
            //Sexta Torre
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 6,
                'ps_id'          => 1,
                'number'         => '506',
            ],

            //Septima Torre
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 7,
                'ps_id'          => 1,
                'number'         => '506',
            ],

            //Octava Torre
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 8,
                'ps_id'          => 1,
                'number'         => '506',
            ],
            //Novena Torre
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 9,
                'ps_id'          => 1,
                'number'         => '506',
            ],

            //Decima Torre
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 10,
                'ps_id'          => 1,
                'number'         => '506',
            ],

            //Decima Primera Torre
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 11,
                'ps_id'          => 1,
                'number'         => '506',
            ],
            //Decima Segunda Torre
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 12,
                'ps_id'          => 1,
                'number'         => '506',
            ],
            //Decima Tercera Torre
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '101',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '102',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '103',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '104',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '105',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '106',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '201',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '202',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '203',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '204',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '205',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '206',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '301',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '302',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '303',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '304',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '305',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '306',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '401',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '402',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '403',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '404',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '405',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '406',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '501',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '502',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '503',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '504',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '505',
            ],
            [
                'building_id'    => 13,
                'ps_id'          => 1,
                'number'         => '506',
            ],

        ];

        Apartment::insert($records);
    }
}
