<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PropertyStatus;

class PropertyStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'             => 1,
                'title'           => 'Habito el inmieble',
            ],
            [
                'id'             => 2,
                'title'           => 'Lo tengo arrendado',
            ],

        ];

        PropertyStatus::insert($records);
    }
}
