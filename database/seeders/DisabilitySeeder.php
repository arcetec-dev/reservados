<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Disability;

class DisabilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'             => 1,
                'title'           => 'Ninguna',
            ],
            [
                'id'             => 2,
                'title'           => 'Sordera',
            ],
            [
                'id'             => 3,
                'title'           => 'Silla de ruedas',
            ],
            [
                'id'             => 4,
                'title'           => 'Ceguera',
            ],
        ];

        Disability::insert($records);
    }
}
