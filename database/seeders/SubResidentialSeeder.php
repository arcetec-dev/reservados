<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SubResidential;

class SubResidentialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'             => 1,
                'residential_id' => 1,
                'name'           => 'Reservado I',
            ],
            [
                'id'             => 2,
                'residential_id' => 1,
                'name'           => 'Reservado II',
            ],
            [
                'id'             => 3,
                'residential_id' => 1,
                'name'           => 'Reservado III',
            ],

        ];

        SubResidential::insert($records);
    }
}
