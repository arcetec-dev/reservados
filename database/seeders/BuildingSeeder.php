<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Building;

class BuildingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'id'             => 1,
                'subresidential_id' => 1,
                'title'           => 'Torre I',
            ],
            [
                'id'             => 2,
                'subresidential_id' => 1,
                'title'           => 'Torre II',
            ],
            [
                'id'             => 3,
                'subresidential_id' => 1,
                'title'           => 'Torre III',
            ],
            [
                'id'             => 4,
                'subresidential_id' => 1,
                'title'           => 'Torre IV',
            ],
            [
                'id'             => 5,
                'subresidential_id' => 1,
                'title'           => 'Torre V',
            ],
            [
                'id'             => 6,
                'subresidential_id' => 1,
                'title'           => 'Torre VI',
            ],
            [
                'id'             => 7,
                'subresidential_id' => 1,
                'title'           => 'Torre VII',
            ],
            [
                'id'             => 8,
                'subresidential_id' => 1,
                'title'           => 'Torre VIII',
            ],
            [
                'id'             => 9,
                'subresidential_id' => 1,
                'title'           => 'Torre IX',
            ],
            [
                'id'             => 10,
                'subresidential_id' => 1,
                'title'           => 'Torre X',
            ],
            [
                'id'             => 11,
                'subresidential_id' => 1,
                'title'           => 'Torre XI',
            ],
            [
                'id'             => 12,
                'subresidential_id' => 1,
                'title'           => 'Torre XII',
            ],
            [
                'id'             => 13,
                'subresidential_id' => 1,
                'title'           => 'Torre XIII',
            ],


        ];

        Building::insert($records);
    }
}
