<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('building_id')->constrained('buildings')->onUpdate('cascade');
            $table->foreignId('user_id')->default(2)->constrained('users')->onUpdate('cascade');
            $table->foreignId('ps_id')->constrained('property_statuses')->onUpdate('cascade')->default(1);
            $table->string('number');
            $table->string('ownername')->nullable();
            $table->string('tenantname')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
