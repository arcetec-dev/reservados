<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_guests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('documenttype_id')->constrained('document_types')->onUpdate('cascade')->default(1);
            $table->bigInteger('gender_id')->constrained('genders')->onUpdate('cascade')->default(1);
            $table->bigInteger('disability_id')->constrained('disabilities')->onUpdate('cascade')->default(1);
            $table->bigInteger('user_id')->constrained('user')->onUpdate('cascade')->default(1);
            $table->string('name');
            $table->string('lastname');
            $table->string('document_number')->nullable();
            $table->string('picture')->nullable();
            $table->date('birth')->default(now());
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_guests');
    }
}
