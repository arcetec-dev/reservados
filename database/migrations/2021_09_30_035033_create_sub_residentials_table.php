<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubResidentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_residentials', function (Blueprint $table) {
            $table->id();
            $table->foreignId('residential_id')->constrained('residentials')->onUpdate('cascade');
            $table->string('name');
            $table->boolean('house')->default(0);
            $table->boolean('building')->default(0);
            $table->boolean('mixed')->default(0);
            $table->string('picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_residentials');
    }
}
