<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('documenttype_id')->constrained('document_types')->onUpdate('cascade')->default(1);
            $table->bigInteger('gender_id')->constrained('genders')->onUpdate('cascade')->default(1);
            $table->bigInteger('disability_id')->constrained('disabilities')->onUpdate('cascade')->default(1);
            $table->string('username')->nullable()->default('SinUsuario');
            $table->string('name');
            $table->string('lastname');
            $table->string('email')->nullable();
            $table->string('document_number')->nullable();
            $table->string('phone')->nullable();
            $table->date('birth')->default(now());
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->foreignId('current_team_id')->nullable();
            $table->text('profile_photo_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
